import Root from './date-field.svelte';
import Input from './date-field-input.svelte';
import Label from './date-field-label.svelte';

export {
    Root,
    Input,
    Label,
    //
    Root as DateField,
    Input as DateFieldInput,
    Label as DateFieldLabel
};
