import Root from './date-range-field.svelte';
import Input from './date-range-field-input.svelte';
import Label from './date-range-field-label.svelte';

export {
    Root,
    Input,
    Label,
    //
    Root as DateRangeField,
    Input as DateRangeFieldInput,
    Label as DateRangeFieldLabel
};
