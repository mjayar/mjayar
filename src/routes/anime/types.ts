export type JikanPaginationItems = {
    count: number;
    total: number;
    per_page: number;
};

export type JikanPagination = {
    last_visible_page: number;
    has_next_page: boolean;
    current_page: number;
    items: JikanPaginationItems;
};

export type JikanResponse<T> = {
    data: T;
    pagination: JikanPagination;
};

export type JikanImagesCollection = {
    image_url: string;
    small_image_url?: string;
    medium_image_url?: string;
    large_image_url?: string;
    maximum_image_url?: string;
};

export type JikanImages = {
    jpg: JikanImagesCollection;
    webp?: JikanImagesCollection;
};

export type AnimeYoutubeVideo = {
    youtube_id: string;
    url: string;
    embed_url: string;
    images?: JikanImagesCollection;
};

export type AnimePromoVideo = {
    title: string;
    trailer: AnimeYoutubeVideo;
};

export type AnimeEpisodeVideo = {
    mal_id: number;
    url: string;
    title: string;
    episode: string;
    images: JikanImages;
};

export type AnimeVideoMeta = {
    title: string;
    author: string;
};

export type AnimeMusicVideo = {
    title: string;
    video: AnimeYoutubeVideo;
    meta: AnimeVideoMeta;
};

export type AnimeVideos = {
    promo: AnimePromoVideo[];
    episodes: AnimeEpisodeVideo[];
    music_videos: AnimeMusicVideo[];
};

export type JikanResource = {
    mal_id: number;
    type: string;
    name: string;
    url: string;
};

export type JikanNamedResource = {
    name: string;
    url: string;
};

export type JikanResourceTitle = {
    type: string;
    title: string;
};

export type JikanResourcePeriod = {
    from: string;
    to: string;
    prop: {
        form: { day: number; month: number; year: number };
        to: { day: number; month: number; year: number };
        string: string;
    };
};

export type JikanResourceRelation = {
    relation: string;
    entry: JikanResource[];
};

export type AnimeBroadcast = {
    day: string;
    time: string;
    timezone: string;
    string: string;
};

export type AnimeTheme = {
    openings: string[];
    endings: string[];
};

export type Anime = {
    mal_id: number;
    url: string;
    images: JikanImages;
    trailer: AnimeYoutubeVideo;
    approved: boolean;
    titles: JikanResourceTitle[];
    title: string;
    title_english: string;
    title_japanese: string;
    title_synonyms: string[];
    type: 'TV' | 'Movie' | 'Ova' | 'Special' | 'Ona' | 'Music';
    source: string;
    episodes: number;
    status: 'Finished Airing' | 'Currently Airing' | 'Complete' | 'Not yet aired';
    airing: boolean;
    aired: JikanResourcePeriod;
    duration: string;
    rating: 'g' | 'pg' | 'pg13' | 'r17' | 'r' | 'rx';
    score: number;
    scored_by: number;
    rank: number;
    popularity: number;
    members: number;
    favorites: number;
    synopsis: string;
    background: string;
    season?: 'winter' | 'spring' | 'summer' | 'fall';
    year: number;
    broadcast: AnimeBroadcast;
    producers: JikanResource[];
    licensors: JikanResource[];
    studios: JikanResource[];
    genres: JikanResource[];
    explicit_genres: JikanResource[];
    themes: JikanResource[];
    demographics: JikanResource[];
    relations?: JikanResourceRelation[];
    theme?: AnimeTheme;
    external?: JikanNamedResource[];
    streaming: JikanNamedResource[];
};
